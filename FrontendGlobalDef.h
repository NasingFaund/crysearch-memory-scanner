#ifndef _CrySearch_FrontendGlobalDef_h_
#define _CrySearch_FrontendGlobalDef_h_

#include "CrySearchWindowManager.h"
#include "CrySearchForm.h"

// Throughout UI components, the window manager may be subject for calling.
extern CrySearchWindowManager* mCrySearchWindowManager;

#endif